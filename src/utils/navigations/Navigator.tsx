import {NavigationContainer} from '@react-navigation/native';
import {appStack} from './Routes';
import * as React from 'react';

export const Navigator = () => {
  return <NavigationContainer>{appStack()}</NavigationContainer>;
};
